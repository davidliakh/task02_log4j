public class ArithmeticOperation {
    private int fOperand;
    private int sOperand;
    public ArithmeticOperation(int a, int b){
      fOperand = a;
      sOperand = b;
    }

    public int divide(){
      return fOperand/sOperand;
    }

    public int multiply(){
      return fOperand*sOperand;
    }
}
