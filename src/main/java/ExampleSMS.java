import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
  public static final String ACCOUNT_SID = "AC0bb6e0086aa6278e4f4355b8e488c6b8";
  public static final String AUTH_TOKEN = "3f45a0c60b720417f721b5783ba380ad";

  public static void send(String text){
    Twilio.init(ACCOUNT_SID,AUTH_TOKEN);
    Message message = Message.creator(new PhoneNumber("+380637295854"), new PhoneNumber("+19188565144"), text).create();
  }
}
